import Bio.SeqIO
import Bio.Seq
import random
#import itertools
import sys
import json
#from tkinter import *
#from Visualize import *

def readReads(path, subset = 1.0, verbose = False):
    reads = [] 
    for read in Bio.SeqIO.parse(path, "fasta"):
        reads.append(str(read.seq))
    n = len(reads)
    
    # Sample subset of Reads


    reads = random.sample(reads, len(reads))


    # Remove duplicates
    reads = list(set(reads))
    nUnique = len(reads)

    # Add reverse complements and remove duplicates again
    reverseComplements = []
    for read in reads:
        reverseComplements.append(Bio.Seq.reverse_complement(read))
    reads = list(set(reads + reverseComplements))
    nComplements = len(reads)

    # Print freedback about results
    #if verbose:

        #print(n, "Reads in", path)
       # print(nSubset, "Reads drawn as subset")
       # print(nUnique, "Reads after removing", nSubset - nUnique,"Duplicates")
       # print(nComplements, "Reads after adding", nComplements - nUnique,
         #     "Reverse Complements")


    return(reads, n, nUnique, nComplements)

def getReads(fnaFile="firstReads2.fna"):
    reads = []

    with open(fnaFile) as f:
        for line in itertools.islice(f, 1, None, 2):
            reads.append(line[:-1])

    return reads

def makeDict(reads, length, typ ="suffix")   :
    if typ == "prefix":
        #prefixList
        keys = [read[length:] for read in reads]
        #suffixList
        values = [read[:length] for read in reads]
    else:
        #suffixList
        values = [read[len(read) - length:] for read in reads]
        # prefixList
        keys = [read[:-length] for read in reads]


    dictionary = {}
    for x in range(len(keys)):
        if dictionary.__contains__(keys[x]):
            temp = dictionary[keys[x]]
            temp.append(values[x])
            dictionary[keys[x]] = temp
        else:
            temp = values[x]
            dictionary[keys[x]] = [temp]


    return dictionary

def makeDicts(reads, maxExtensionLength, typ = "prefix"):
    result = {}
    for i in range(1, maxExtensionLength+1):
        result.update(makeDict(reads, i,     typ))
    return result

def findExtensions(contig, reads, dictionary, typ = "prefix", 
                         maxExtLen = 10, maxConflictLen = 30):
    
    readLen = len(reads[0])
    extensions = []
    subContigs = []

    # check extensions by maxConflictLen to maxExtLen (30-11) for conflicts
    for i in range(maxConflictLen, maxExtLen, -1):
        if typ == "suffix":
            subContig = contig[(-readLen + i):]
        else:
            subContig = contig[:(readLen)-i]
        if subContig in dictionary:
            extension = dictionary[subContig]
            if len(extension) > 1:
                #print("conflict", extension)
                conflictPositionsX = []
                for j in range(len(extension)-1):
                    for k in range(len(extension[j])):
                        if extension[j][k] != extension[j+1][k]:
                            conflictPositionsX.append(k)
                            conflictPositionsDict[subContig] = conflictPositionsX
                #print(conflictPositionsX)
                conflictingSubContigs.append(subContig)

                return 0,0

    # check extensions by maxExtLen to zero (10 - 1) for conflicts and
    # keep possible extensions in list
    for i in range(maxExtLen, 0, -1):
        if typ == "suffix":
            subContig = contig[(-readLen + i):]
        else:
            subContig = contig[:(readLen)-i]
        if subContig in dictionary:
            extension = dictionary[subContig]
            if len(extension) > 1:
                return 0,0
            else:
                extLengths.append(len(extension[0]))
                extensions.append(extension[0])
                subContigs.append(subContig)

    # build reads corresponding to extensions
    if typ == "suffix":
        extensionReads = [c + e for c, e in zip(subContigs, extensions)]
    if typ == "prefix":
        extensionReads = [e + c for c, e in zip(subContigs, extensions)]

    # check if reads still available
    for extensionRead, extension in zip(extensionReads[:], extensions[:]):
        if extensionRead not in reads:
            extensionReads.remove(extensionRead)
            extensions.remove(extension)

    if len(extensions) == 0:
        return 0,0
    else:
        return(extensions, extensionReads)

def extendContig(contig, reads, pDict, sDict, maxExtLen, maxConflictLen):
    #print("Extend contig to the right")
    while (True):
        if len(reads) == 0:
            break
        else:
            extensions, extensionReads = findExtensions(contig, reads, sDict,
                                                        "suffix",maxExtLen,
                                                        maxConflictLen)
            if extensions == 0:
                break
            else:
                for extensionRead in extensionReads:
                    reads.remove(extensionRead)

                contig += extensions[0]

    #print("Extend contig to the left")
    while (True):
        if len(reads) == 0:
            break
        else:
            extensions, extensionReads = findExtensions(contig, reads, pDict,
                                                        "prefix",maxExtLen,
                                                        maxConflictLen)
            if extensions == 0:
                break
            else:
                for extensionRead in extensionReads:
                    reads.remove(extensionRead)

                contig = extensions[0] + contig

    return contig

def getAllContigs(reads, suffixDictionary, prefixDictionary, maxExtLen, maxConflictLen, verbose = True):
    #print("length",len(reads))
    while len(reads) > 0:
        contig = reads.pop(random.randint(0, len(reads)-1))
        extendedReads.append(contig)
        contig = extendContig(contig, reads, prefixDictionary, suffixDictionary, 
                              maxExtLen, maxConflictLen)
        contigs.append(contig)
        decreasingNumberOfReads.append(len(reads))

    return contigs

def compareContigs(contigList, refG):
    conigPositions = []
    conigLengths = []
    matchingContigs = []
    for contig in contigList:
        if contig in refG:
            conigPositions.append(refG.index(contig))
            conigLengths.append(len(contig))
            matchingContigs.append(contig)
    return conigPositions, conigLengths, matchingContigs

def n50(contigList, percentage):
    n=0
    esitmateSize = 0
    for contig in contigList:
        esitmateSize += len(contig)

    newContigs = []
    for contig in contigList:
        if contig >= esitmateSize*percentage:
            newContigs.append(contigList)
    newContigs.sort()
    #print(newContigs)
    n = len(newContigs[0])
    return n

if __name__ == "__main__":
    path = sys.argv[1]
    reads, n, nUnique, nComplements = readReads(path, verbose = True)

    mELen = int(sys.argv[2])
    mCfLen = int(sys.argv[3])
    suffixDictionary = makeDicts(reads, mCfLen, "suffix")
    prefixDictionary = makeDicts(reads, mCfLen, "prefix")
    extendedReads = []
    decreasingNumberOfReads = []
    conflictingSubContigs = []
    conflictPositionsDict = {}

    extLengths = []
    contigs = []

    contigList = getAllContigs(reads, suffixDictionary, prefixDictionary, mELen, mCfLen)
    #print(contigList)
    #refG = str(Bio.SeqIO.read("chr11_region.fna","fasta").seq)
    #lists = compareContigs(contigList, refG)
    #print(decreasingNumberOfReads, conflictPositionsDict)


    #for contig in contigList:
        #print(len(contig))
    #print("1",json.dumps(decreasingNumberOfReads))
   # print("2",json.dumps(conflictPositionsDict))
    #print("3",json.dumps(mCfLen))

    everything = {"mELen": mELen, "mCfLen": mCfLen, "suffixDictionary": suffixDictionary, "prefixDictionary": prefixDictionary,
    "extendedReads": extendedReads, "decreasingNumberOfReads": decreasingNumberOfReads,
     "conflictingSubContigs": conflictingSubContigs, "conflictPositionsDict": conflictPositionsDict,"contigList":contigList}

    everythingInJSON = json.dumps(everything)
    print (everythingInJSON)



    #a = json.dumps(decreasingNumberOfReads)
    #b = json.dumps(conflictPositionsDict)
    #c = dict(a.items() + b.items())
    #print("c",c)


    #root = Tk()
    #viz = Visualize(root,n, nUnique, nComplements, extendedReads, decreasingNumberOfReads, suffixDictionary, prefixDictionary, conflictingSubContigs, conflictPositionsDict,extLengths, contigs, mELen, mCfLen)
