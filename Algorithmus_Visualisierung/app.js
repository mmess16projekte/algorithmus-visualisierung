/*eslint-env node */
(function() {
    var childProcess = require("child_process");
    var oldSpawn = childProcess.spawn;
    function mySpawn() {
        console.log('spawn called');
        console.log(arguments);
        var result = oldSpawn.apply(this, arguments);
        return result;
    }
    childProcess.spawn = mySpawn;
})();
var express = require("express"),
    PythonShell = require("python-shell"),
    handlebars = require('express-handlebars').create({defaultLayout: 'main'}),
    formidable = require('formidable'),
    options = { mode: 'text', args: []},
    args = [],

    app;
global.latestResults = {};
global.options = { mode: 'text', args: []};

function onUserCalledRunRoute(req, res) {
    var pyshell = new PythonShell(req.params.file);
    pyshell.on("message", function (message) {
        res.send(message);
    });
}

function onUserRequestAvailableFiles(req, res) {
    var files = ["python.py"];

    res.send(files);
}


function start() {

    app = express();
    app.disable('x-powered-by');


    app.engine('handlebars', handlebars.engine);

    app.set('view engine', 'handlebars');

    app.use(require('body-parser').urlencoded({extended: true}));

    app.use('/static', express.static('public'));


    app.get('/latestResults', function (req, res) { res.send (global.latestResults); });

    app.get('/', function (req, res) {
        res.render('home');
    });
    app.get('/about', function (req, res) {
        res.render('about');
    });
    app.use(function (req, res, next) {
        console.log("Looking for URL : " + req.url);
        console.log('Time:', Date.now());
        next();
    });

    app.use(function (err, req, res, next) {
        console.log('Error : ' + err.message);
        next();
    });


    app.get('/thankyou', function (req, res) {
        res.render('thankyou');
    });
    app.get("/run/:file", onUserCalledRunRoute);
    app.get("/scripts", onUserRequestAvailableFiles);

    app.get('/file-upload', function (req, res) {
        res.render('file-upload');
    });
    app.post('/file-upload',
        function (req, res) {

            var form = new formidable.IncomingForm();
            form.parse(req, function (err, fields, file) {
                if (err) {
                    return res.redirect(303, '/error');
                }
                global.latestResults = {};
                console.log("fields", fields);
                args = ["./FASTA files/"+fields["selected file"]+".fna","1", fields["Max Conflict Search Length"]];

                options.args = args;
                console.log(file);
                var timeStamp = Date.now();
                PythonShell.run('Assembler.py', options, function (err, results) {
                    if(err){
                        throw err;
                    }
                    var data = JSON.parse(results);
                    global.latestResults[timeStamp] = data;
                    res.redirect(303, '/visualization/result='+timeStamp);

                });


            });
        });

    app.get('/visualization/:timestamp', function (req, res) {
        console.log("render");
        res.render('visualization');
    });

    app.use(function (req, res) {
        res.type('text/html');
        res.status(404);
        res.render('404');
    });

    app.use(function (err, req, res, next) {
        console.error(err.stack);
        res.status(500);
        res.render('500');
    });

    app.listen(8888);
}
start();
