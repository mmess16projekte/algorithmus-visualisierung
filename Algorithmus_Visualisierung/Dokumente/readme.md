Starting the app:
Option A:
	visit: https://htgenomeassembler.herokuapp.com/
Option B:
	1. Start the app from the repository with the command „node app“
	2. visit: http://localhost:8888

Short description:
The application aims to visualize an algorithm, that is used to assemble genomes.
The goal is to help Bioinformatics students have a better understanding of the algorithm.

The genome is fragmented into reads, which can be understood as small snippets of the genome (e.g. ATGCGCTAGCGTGC). These reads are then compared with each other using Hashtables to extend them to contigs(longer snippets).
