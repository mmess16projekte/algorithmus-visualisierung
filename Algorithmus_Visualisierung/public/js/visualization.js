/* global $ */

Visualization = function () {
    "use strict";

    var data = {},
        suffixDictionary = {},
        contigs = [],
        maxExtentionLength = 1,
        maxConflictFindLength = 0,
        prefixDictionary = {},
        conflictPositionsDict = {},
        readLen = 0,
        subContigs = [],
        extentions = [],
        extendedReads = [],
        step = 0,
        currentContig = 0,
        suffixExtention = true,
        extendedRight = false,
        bremse = 0;


    function visualize(contig, extendedRead){
        subContigs = [];
        var conflictPositions = undefined;
        var positionOfExtendedRead = contig.indexOf(extendedRead);
        var space = "...";

        if(suffixExtention){
            createExtentions(contig.substring(positionOfExtendedRead),step, suffixExtention, positionOfExtendedRead);
            conflictPositions = adhocConflictPos(extentions);
        }else {
            createExtentions(contig.substring(0, positionOfExtendedRead+readLen),step, suffixExtention, positionOfExtendedRead); // -1?
            //adhocConflictPosReverse(extentions);
        }

        if(extendedRight && suffixExtention){
            showFinishedContig();
            extendedRight = false;
            suffixExtention = true;
            currentContig += 1;
            step = 0;
            return;
        }

        if(extendedRight === true && !suffixExtention){
            extendedRight = false;
            step = 0;
            if (bremse <5){
                bremse++;
                visualize(contig,extendedRead);
                return;
            }else {
            }

        }
        $(".header").append("<p>"+"Start Read: " + contig.substring(positionOfExtendedRead,positionOfExtendedRead+readLen) + ", extensions max: " + data["mELen"]+"</p>");
        $(".header").append("<p>Current contig: </p>");
        if(suffixExtention){
            $(".header").append("<p>"+space + contig.substring(positionOfExtendedRead+step+2,positionOfExtendedRead-1+readLen+step) +"</p>"); // -1?

            extendRight(conflictPositions);
        }else{
            $(".header").append("<p id = 'currentContig'>"+ reverseContig(contig.substring(positionOfExtendedRead+step+2,positionOfExtendedRead+2+readLen+step))+space +"</p>"); // -1?
            $("#currentContig").css("margin-left", 10 * (maxConflictFindLength) + 50 + "px");

            extendLeft(conflictPositions);
        }
        step += 1;
    }

    function createExtentions(contig, step, suffix, positionOfExtendedRead) {
        extentions = [];
        for(var i = 0; i < maxConflictFindLength;i++){

            var subContig = "";
            var exitCondition;

            if(suffix){
                subContig = contig.substring(step+i, readLen-1+step);

                exitCondition = subContig === contig.substring(contig.length-subContig.length);
            }else{
                contig = reverseContig(contig);
                subContig = contig.substring(step+i, readLen-1+step);
                subContig = reverseContig(subContig);
                contig = reverseContig(contig);

                exitCondition = subContig === contig.substring(0,subContig.length);
            }

            if(exitCondition){
                if(positionOfExtendedRead === 0){
                    suffixExtention = true;
                }else {
                    suffixExtention = false;
                }

                if (positionOfExtendedRead !== 0 && !suffix){
                    suffixExtention = true;
                }
                extendedRight = true;

                return;
            }
            try{
                if(suffix){
                    extentions.push(suffixDictionary[subContig][0]);
                }else{
                    extentions.push(prefixDictionary[subContig][0]); //vielleicht doch nur suffix
                }
            }
            catch(err) {
                extentions.push("");
            }
            subContigs.push(subContig);

        }
    }

    function extendRight(conflictPositions) {
        for(var i = 0; i < maxConflictFindLength; i++){
            var elem = $("<p id = 'extention'>"+subContigs[i]+" - "+highlight(subContigs[i], extentions[i],conflictPositions)+"</p>");
            if(i===0){
                elem.css("margin-left", 30 * i + "px");
            }else {
                elem.css("margin-left", 10 * i + "px");
            }
            $(".extentions").append(elem);

            if(suffixDictionary[subContigs[i]] !== undefined){
                if(suffixDictionary[subContigs[i]].length > 1){
                    var elem = $("<p id = 'extention'>"+subContigs[i]+"  -  "+highlight(subContigs[i], suffixDictionary[subContigs[i]][1],conflictPositions)+"</p>");
                    elem.css("margin-left", 10 * i + "px");
                    elem.css({opacity: 0.4});
                    $(".extentions").append(elem);
                }
            }

        }
    }

    function extendLeft(conflictPositions) {
        for(var i = 0; i < maxConflictFindLength; i++){
            var elem = $("<p id = 'extention'>"+highlight(subContigs[i], extentions[i],conflictPositions)+" - "+subContigs[i]+"</p>");

            elem.css("margin-left", 10 * (maxConflictFindLength-i) + "px");

            $(".extentions").append(elem);
            if(prefixDictionary[subContigs[i]] !== undefined){
                if(prefixDictionary[subContigs[i]].length > 1){
                    var elem = $("<p id = 'extention'>"+highlight(subContigs[i], prefixDictionary[subContigs[i]][1],conflictPositions)+"  -  "+subContigs[i]+"</p>");
                    elem.css("margin-left", 10 * (maxConflictFindLength-i) + "px");
                    elem.css({opacity: 0.4});
                    $(".extentions").append(elem);
                }
            }

        }
    }

    function highlight(subContig, extention, conflictPositions) {
        var char = extention.charAt(0);
        var element = "<spawn class = 'markGreen'>" + char + "</spawn>";
        var result = "<spawn>" + "</spawn>" + element + "<spawn>" + extention.slice(1, extention.length) + "</spawn>";


        if (conflictPositions !== undefined){

            result = "";

            for (var i = 0; i < extention.length;i++){
                if(_.contains(conflictPositions, i)){
                    char = extention[i];
                    result += "<spawn class = 'markRed'>" + char + "</spawn>";
                }else{
                    char = extention[i];
                    result += "<spawn>" + char + "</spawn>";
                    if(!_.contains(conflictPositions, 0) && i===0){
                        result = "<spawn class = 'markGreen'>" + char + "</spawn>";
                    }
                }

            }
        }

        return result
    }

    function adhocConflictPos(extentions){
        var conflictPos = [];
        var longestExtension = extentions[extentions.length-1];
        for(var i = 0; i < extentions.length; i++){
            for(var j = 0; j < extentions[i].length; j++){
                if (extentions[i][j] !== longestExtension[j]){
                    conflictPos.push(j);
                }
            }
        }
        conflictPos = _.uniq(conflictPos);
        conflictPos.sort();
        return conflictPos;
    }

    function adhocConflictPosReverse(extentions){
        var conflictPos = [];
        var longestExtension = extentions[extentions.length-1];
        for(var i = extentions.length-1; i > 0; i++){
            for(var j = extentions[i].length; j > 0; j++){
                if (extentions[i][j] !== longestExtension[j]){
                    conflictPos.push(j);
                }
            }
        }
        conflictPos = _.uniq(conflictPos);
        conflictPos.sort();
        return conflictPos;
    }


    function showFinishedContig() {
        $(".header").append($("<p>finished Contig: " + contigs[currentContig]+"</p>"));
        $(".extentions").append($("<p>next Contig: " + contigs[currentContig+1]+"</p>"));

    }

    function reverseContig(contig) {
        return contig.split("").reverse().join("");
    }

    function nextStep(){
        $(".extentions").empty();
        $(".header").empty();
        visualize(contigs[currentContig], extendedReads[currentContig]);
    }

    function init(latestResults){
        data = latestResults;
        contigs = data["contigList"];
        contigs.reverse();
        _.reject(contigs, function (contig) {
            return contig.length === 32;
        });
        extendedReads = data["extendedReads"];
        extendedReads.reverse();
        maxExtentionLength = data["mELen"];
        maxConflictFindLength = data["mCfLen"];
        suffixDictionary = data["suffixDictionary"];
        prefixDictionary = data["prefixDictionary"];
        conflictPositionsDict = data["conflictPositionsDict"];
        readLen = extendedReads[0].length;
        visualize(contigs[currentContig], extendedReads[currentContig]);
    }

    $(document).ready(function (){
        $("#next").focus();
        $.get( '/latestResults', function (latestResults) {
            init(latestResults[Object.keys(latestResults)[0]]);
        });
    });

    $("#next").click(function(){
        nextStep();
    });

};

Visualization();